CREATE TABLE "Staging_mockup_data"."PARTY_ROLE_APPL" (
	"UUID" varchar(1200) NULL,
	"PKEY_SRC" varchar(1020) NULL,
	"PARTY_PKEY_SRC" varchar(1020) NULL,
	"APPLN_PKEY_SRC" varchar(1020) NULL,
	"ENTITY_CD" varchar(40) NULL,
	"AFFILIATE_CD" varchar(40) NULL,
	"CERTIFICATE_NO" varchar(40) NULL,
	"BENEFICIARY_ID" varchar(128) NULL,
	"DEPENDENT_NO" varchar(12) NULL,
	"PARTY_ROLE_CD" varchar(40) NULL,
	"DEL_FLG" varchar(4) NULL,
	"SOURCE_ROWID" varchar(60) NULL,
	"SRC_ROWID" int4 NULL,
	"SRC_SYSTEM_CD" varchar(40) NULL,
	"TRANSACTION_FLAG" varchar(4) NULL,
	"BATCH_ID" varchar(40) NULL,
	"LAST_UPDATE_DATE" timestamp NULL
);