CREATE TABLE "Staging_mockup_data"."TELEPHONE_CONTACT" (
	"UUID" varchar(1200) NULL,
	"PKEY_SRC" varchar(1020) NULL,
	"PARTY_PKEY_SRC" varchar(1020) NULL,
	"ENTITY_CD" varchar(40) NULL,
	"AREA_CD" varchar(40) NULL,
	"COUNTRY_CALLING_CD" varchar(40) NULL,
	"TELEPHONE_ADDR_TYPE_CD" varchar(40) NULL,
	"TELEPHONE_NO" varchar(400) NULL,
	"EXTENSION_NO" int4 NULL,
	"DEL_FLG" varchar(4) NOT NULL,
	"SOURCE_ROWID" varchar(60) NULL,
	"SRC_ROWID" int4 NULL,
	"SRC_SYSTEM_CD" varchar(40) NULL,
	"TRANSACTION_FLAG" varchar(4) NULL,
	"BATCH_ID" varchar(40) NULL,
	"LAST_UPDATE_DATE" timestamp NULL
);