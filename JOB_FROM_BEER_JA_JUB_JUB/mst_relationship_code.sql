INSERT INTO "Landing_G400_Temp".mst_relationship_code ("PARTY_1","DPNTNO_1","CLTSEX_1","PARTY_2","DPNTNO_2","CLTSEX_2","CODE","MEANING","FLAG","CREATE_DATE","UPDATE_DATE") VALUES
	 ('Policy Holder','','','Affiliate Company','','','PHAFF','Party 1 เป็นบริษัทแม่ของ Party 2','Y','',''),
	 ('Affiliate Company','','','Policy Holder','','','AFFPH','Party 1 เป็นบริษัทลูกของ Party 2','Y','',''),
	 ('Employee','','','Policy Holder','','','EMPPH','Party 1 เป็นพนักงานของบริษัทแม่ Party 2','Y','',''),
	 ('Policy Holder','','','Employee','','','PHEMP','Party 1 เป็นบริษัทแม่ของพนักงาน Party 2','Y','',''),
	 ('Employee','','','Affiliate Company','','','EMPAFF','Party 1 เป็นพนักงานของบริษัทลูก Party 2','Y','',''),
	 ('Affiliate Company','','','Employee','','','AFFEMP','Party 1 เป็นบริษัทลูกของพนักงาน Party 2','Y','',''),
	 ('Employee','00','F','Dependent (Husband)','01','M','WIFE','Party 1 เป็นภรรยาของ Party 2','Y','',''),
	 ('Employee','00','F','Dependent (Husband)','31-40','M','WIFE','Party 1 เป็นภรรยาของ Party 2','Y','',''),
	 ('Dependent (Husband)','01','M','Employee','00','F','HUSB','Party 1 เป็นสามีของ Party 2','Y','',''),
	 ('Dependent (Husband)','31-40','M','Employee','00','F','HUSB','Party 1 เป็นสามีของ Party 2','Y','','');
INSERT INTO "Landing_G400_Temp".mst_relationship_code ("PARTY_1","DPNTNO_1","CLTSEX_1","PARTY_2","DPNTNO_2","CLTSEX_2","CODE","MEANING","FLAG","CREATE_DATE","UPDATE_DATE") VALUES
	 ('Employee','00','M','Dependent (Spouse)','01','U or (Blank)','SPOUSE','Party 1 เป็นคู่สมรสของ Party 2','Y','',''),
	 ('Employee','00','M','Dependent (Spouse)','31-40','U or (Blank)','SPOUSE','Party 1 เป็นคู่สมรสของ Party 2','Y','',''),
	 ('Employee','00','F','Dependent (Spouse)','01','U or (Blank)','SPOUSE','Party 1 เป็นคู่สมรสของ Party 2','Y','',''),
	 ('Employee','00','F','Dependent (Spouse)','31-40','U or (Blank)','SPOUSE','Party 1 เป็นคู่สมรสของ Party 2','Y','',''),
	 ('Employee','00','U or (Blank)','Dependent (Spouse)','01','U or (Blank)','SPOUSE','Party 1 เป็นคู่สมรสของ Party 2','Y','',''),
	 ('Employee','00','U or (Blank)','Dependent (Spouse)','31-40','U or (Blank)','SPOUSE','Party 1 เป็นคู่สมรสของ Party 2','Y','',''),
	 ('Dependent (Spouse)','01','U or (Blank)','Employee','00','U or (Blank)','SPOUSE','Party 1 เป็นคู่สมรสของ Party 2','Y','',''),
	 ('Dependent (Spouse)','31-40','U or (Blank)','Employee','00','U or (Blank)','SPOUSE','Party 1 เป็นคู่สมรสของ Party 2','Y','',''),
	 ('Employee','00','M','Dependent (Son)','02-30','M','DAD','Party 1 เป็นพ่อของ Party 2','Y','',''),
	 ('Dependent (Son)','02-30','M','Employee','00','M','SON','Party 1 เป็นลูกชายของ Party 2','Y','','');
INSERT INTO "Landing_G400_Temp".mst_relationship_code ("PARTY_1","DPNTNO_1","CLTSEX_1","PARTY_2","DPNTNO_2","CLTSEX_2","CODE","MEANING","FLAG","CREATE_DATE","UPDATE_DATE") VALUES
	 ('Employee','00','F','Dependent (Son)','02-30','M','MOM','Party 1 เป็นแม่ของ Party 2','Y','',''),
	 ('Dependent (Son)','02-30','M','Employee','00','F','SON','Party 1 เป็นลูกชายของ Party 2','Y','',''),
	 ('Employee','00','M','Dependent (Daughter)','02-30','F','DAD','Party 1 เป็นพ่อของ Party 2','Y','',''),
	 ('Dependent (Daughter)','02-30','F','Employee','00','M','DAU','Party 1 เป็นลูกสาวของ Party 2','Y','',''),
	 ('Employee','00','F','Dependent (Daughter)','02-30','F','MOM','Party 1 เป็นแม่ของ Party 2','Y','',''),
	 ('Dependent (Daughter)','02-30','F','Employee','00','F','DAU','Party 1 เป็นลูกสาวของ Party 2','Y','',''),
	 ('Employee','00','U or (Blank)','Dependent (Son)','02-30','M','PARE','Party 1 เป็นพ่อแม่ของ Party 2','Y','',''),
	 ('Dependent (Son)','02-30','M','Employee','00','U or (Blank)','SON','Party 1 เป็นลูกชายของ Party 2','Y','',''),
	 ('Employee','00','U or (Blank)','Dependent (Daughter)','02-30','F','PARE','Party 1 เป็นพ่อแม่ของ Party 2','Y','',''),
	 ('Dependent (Daughter)','02-30','F','Employee','00','U or (Blank)','DAU','Party 1 เป็นลูกสาวของ Party 2','Y','','');
INSERT INTO "Landing_G400_Temp".mst_relationship_code ("PARTY_1","DPNTNO_1","CLTSEX_1","PARTY_2","DPNTNO_2","CLTSEX_2","CODE","MEANING","FLAG","CREATE_DATE","UPDATE_DATE") VALUES
	 ('Employee','00','M','Dependent (Child)','02-30','U or (Blank)','DAD','Party 1 เป็นพ่อของ Party 2','Y','',''),
	 ('Dependent (Child)','02-30','U or (Blank)','Employee','00','M','CHILD','Party 1 เป็นลูกของ Party 2','Y','',''),
	 ('Employee','00','F','Dependent (Son)','02-30','U or (Blank)','MOM','Party 1 เป็นแม่ของ Party 2','Y','',''),
	 ('Dependent (Child)','02-30','U or (Blank)','Employee','00','F','CHILD','Party 1 เป็นลูกของ Party 2','Y','',''),
	 ('Employee','00','U or (Blank)','Dependent (Child)','02-30','U or (Blank)','PARE','Party 1 เป็นพ่อแม่ของ Party 2','Y','',''),
	 ('Dependent (Child)','02-30','U or (Blank)','Employee','00','U or (Blank)','CHILD','Party 1 เป็นลูกของ Party 2','Y','',''),
	 ('Employee','00','M','Dependent (Father)','41-50','M','SON','Party 1 เป็นลูกชายของ Party 2','Y','',''),
	 ('Dependent (Father)','41-50','M','Employee','00','M','DAD','Party 1 เป็นพ่อของ Party 2','Y','',''),
	 ('Employee','00','F','Dependent (Father)','41-50','M','DAU','Party 1 เป็นลูกสาวของ Party 2','Y','',''),
	 ('Dependent (Father)','41-50','M','Employee','00','F','DAD','Party 1 เป็นพ่อของ Party 2','Y','','');
INSERT INTO "Landing_G400_Temp".mst_relationship_code ("PARTY_1","DPNTNO_1","CLTSEX_1","PARTY_2","DPNTNO_2","CLTSEX_2","CODE","MEANING","FLAG","CREATE_DATE","UPDATE_DATE") VALUES
	 ('Employee','00','F','Dependent (Mother)','41-50','F','DAU','Party 1 เป็นลูกสาวของ Party 2','Y','',''),
	 ('Dependent (Mother)','41-50','F','Employee','00','F','MOM','Party 1 เป็นแม่ของ Party 2','Y','',''),
	 ('Employee','00','U or (Blank)','Dependent (Father)','41-50','M','CHILD','Party 1 เป็นลูกของ Party 2','Y','',''),
	 ('Dependent (Father)','41-50','M','Employee','00','U or (Blank)','DAD','Party 1 เป็นพ่อของ Party 2','Y','',''),
	 ('Employee','00','U or (Blank)','Dependent (Mother)','41-50','F','CHILD','Party 1 เป็นลูกของ Party 2','Y','',''),
	 ('Dependent (Mother)','41-50','F','Employee','00','U or (Blank)','MOM','Party 1 เป็นแม่ของ Party 2','Y','',''),
	 ('Dependent (Spouse)','01','U or (Blank)','Employee','00','F','SPOUSE','Party 1 เป็นคู่สมรสของ Party 2','Y','',''),
	 ('Dependent (Spouse)','31-40','U or (Blank)','Employee','00','F','SPOUSE','Party 1 เป็นคู่สมรสของ Party 2','Y','',''),
	 ('Employee','00','M','Dependent (Wife)','01','F','HUSB','Party 1 เป็นสามีของ Party 2','Y','',''),
	 ('Employee','00','M','Dependent (Wife)','31-40','F','HUSB','Party 1 เป็นสามีของ Party 2','Y','','');
INSERT INTO "Landing_G400_Temp".mst_relationship_code ("PARTY_1","DPNTNO_1","CLTSEX_1","PARTY_2","DPNTNO_2","CLTSEX_2","CODE","MEANING","FLAG","CREATE_DATE","UPDATE_DATE") VALUES
	 ('Dependent (Wife)','01','F','Employee','00','M','WIFE','Party 1 เป็นภรรยาของ Party 2','Y','',''),
	 ('Dependent (Wife)','31-40','F','Employee','00','M','WIFE','Party 1 เป็นภรรยาของ Party 2','Y','',''),
	 ('Employee','00','M','Dependent (Mother)','41-50','F','SON','Party 1 เป็นลูกชายของ Party 2','Y','',''),
	 ('Dependent (Mother)','41-50','F','Employee','00','M','MOM','Party 1 เป็นแม่ของ Party 2','Y','',''),
	 ('Employee','00','M','Dependent (Parent)','41-50','U or (Blank)','SON','Party 1 เป็นลูกชายของ Party 2','Y','',''),
	 ('Dependent (Parent)','41-50','U or (Blank)','Employee','00','M','PARE','Party 1 เป็นพ่อแม่ของ Party 2','Y','',''),
	 ('Employee','00','F','Dependent (Parent)','41-50','U or (Blank)','DAU','Party 1 เป็นลูกสาวของ Party 2','Y','',''),
	 ('Dependent (Parent)','41-50','U or (Blank)','Employee','00','F','PARE','Party 1 เป็นพ่อแม่ของ Party 2','Y','',''),
	 ('Employee','00','U or (Blank)','Dependent (Parent)','41-50','U or (Blank)','CHILD','Party 1 เป็นลูกของ Party 2','Y','',''),
	 ('Dependent (Parent)','41-50','U or (Blank)','Employee','00','U or (Blank)','PARE','Party 1 เป็นพ่อแม่ของ Party 2','Y','','');
INSERT INTO "Landing_G400_Temp".mst_relationship_code ("PARTY_1","DPNTNO_1","CLTSEX_1","PARTY_2","DPNTNO_2","CLTSEX_2","CODE","MEANING","FLAG","CREATE_DATE","UPDATE_DATE") VALUES
	 ('Dependent (Spouse)','01','U or (Blank)','Employee','00','M','SPOUSE','Party 1 เป็นคู่สมรสของ Party 2','Y','',''),
	 ('Dependent (Spouse)','31-40','U or (Blank)','Employee','00','M','SPOUSE','Party 1 เป็นคู่สมรสของ Party 2','Y','','');
