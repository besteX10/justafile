package routines;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class get_salutation {
	

	String[] salList;
	String input;
	
    public get_salutation(String input, String[] salList){
        this.input = input;
        this.salList = salList;
    }

    public static String check(String input, String[] salList) {
		get_salutation global_salList = new get_salutation(input, salList);
		if (input == null) {
			return null;
		}
		else {
			input = input.toUpperCase().trim();
			input = input.replaceAll("\\s+", " ");
			//Pattern pattern = Pattern.compile("\\d{3}-\\d{7}");
			//Matcher matcher = pattern.matcher(input);
			//if (matcher.find()) {
			//	return input.toUpperCase();
			//}
			if (input.length() > 0) {
				for (int i = 0; i < input.length(); i++) {
					for (String search: global_salList.salList) {
						search = search.toUpperCase().trim();
						if (input.contains(search)) {
							int count = (input.split(search, -1).length) - 1;
							String[] appended = new String[input.length() + 1];
							for (int j = 0; j < input.length(); j++) {
								appended[j] = input;
								appended[j+1] = " ";
							}
							if (count > 1) {
								StringBuilder fname = new StringBuilder(input);
								fname.replace(0, search.length(), "");
								if (input.length() >= 2) {
									String str_fname = fname.toString();
									return str_fname.trim().toUpperCase();
								}
								else {
									return fname.toString().trim().toUpperCase();
								}
							}
							else if (input.length() == 3) {
								String fname = input.replaceAll(search, "");
								fname = input + " " + input;
								fname = fname.replaceAll("(^\\h*)|(\\h*$)", "");
								return fname.trim().toUpperCase();
							}
							else if (input.equals(search) && input.length() > 1) {
								return input.trim().toUpperCase();
							}
							else if (input.length() == 2) {
								String fname = input.replaceAll(search, "");
								fname = fname + " " + appended[appended.length - 2];
								fname = fname.replaceAll("(^\\h*)|(\\h*$)", "");
								return fname.trim().toUpperCase();
							} else {
								if (input.toString().startsWith(search)) {
									String escapedSearch = Pattern.quote(search);
									String fname = input.replaceAll(escapedSearch, "");
									return fname.trim().toUpperCase();
								}
							}
						}
					}
				}
			}
		}
		return input.toUpperCase();
	}

    public static String split_sal(String input, String[] salList) {
		if (input == null) {
			return null;
		}
		input = input.toUpperCase().trim();
		get_salutation global_salList = new get_salutation(input, salList);
		if (input.length() > 0) {
			for (int i = 0; i < input.length(); i++) {
				for (String search: global_salList.salList) {
					search = search.toUpperCase().trim();
					if (input.contains(search) && input.startsWith(search)) {
						int count = (input.split(search, -1).length) -1;
						if (count > 1) {
							return search.toUpperCase();
						}
						else if (input.equals(search)) {
							return search;
						}
						else if (count == 1) {
							return search.toUpperCase();
						}
						else {
							if (input.indexOf(search) < 1) {
								return search.toUpperCase();
							}
							else {
								return null;
							}
						}
					}
				}
			}
		}
		return null; 
	}

	public static String check_g400(String input, String[] salList) {
	
	    if (input == null) {
	        return input;
	    }else{
	    	input = input.replaceAll("\\s+"," ");
	    	
	    	Pattern pattern = Pattern.compile("\\d{3}-\\d{7}");
	        Matcher matcher = pattern.matcher(input);
	        if(matcher.find()){
	            return input;
	        }
	    	
	        String[] arr_input = input.split("\\s");
	        get_salutation global_salList = new get_salutation(input, salList);
	
	        if (arr_input.length > 0) {
	            for (int i = 0; i < arr_input.length; i++) {
	                for (String search : global_salList.salList) {
	                    if (arr_input[0].contains(search)) {
	                        int count = (arr_input[0].split(search, -1).length) - 1;
	
	                        String[] appended = new String[arr_input.length + 1];
	                        for (int j = 0; j < arr_input.length; j++) {
	                            appended[j] = arr_input[j];
	                            appended[j + 1] = " ";
	                        }
	
	                        if(count > 1){
	                            StringBuilder fname = new StringBuilder(arr_input[0]);
	                            fname.replace(0, search.length(), "");
	                            if (arr_input.length >= 2) {
	                                String str_fname = fname.toString().toUpperCase();
	                                str_fname = str_fname + " " + appended[appended.length - 2];
	                                return str_fname;
	                            }else{
	                                return fname.toString().toUpperCase();
	                            }
	                        }else if(arr_input.length == 3) {
	                            String fname = arr_input[0].replaceAll(search, "");
	                            fname = arr_input[1] + " " + arr_input[arr_input.length - 1];
	                            return fname.toUpperCase();
	                        }else if(arr_input[0].equals(search)) {
	                        	for(String search_2 : salList) {
	                        		if(arr_input[arr_input.length - 1].contains(search_2)) {
	                        			String output = arr_input[arr_input.length - 1].replaceAll(search_2, "");
	                        			return output.toString().toUpperCase();
	                        		}
	                        	}
	                            return arr_input[1];
	                        }else if(arr_input.length == 2){
	                            String fname = arr_input[0].replaceAll(search, "");
	                            fname = fname + " " + appended[appended.length - 2];
	                            return fname.toUpperCase();
	                        }else{
	                            String fname = arr_input[0].replaceAll(search, "");
	                            return fname.toUpperCase();
	                        }
	                    }
	                }
	            }
	        }
	    }
	    return input;
	}

    public static String get_customer_type(String input, String[] salList, String[] comList){
        for (int i = 0; i < salList.length; i++){
        	salList[i] = salList[i].toUpperCase();
        	input = input.toUpperCase();
          for (int j = 0; j < comList.length; j++){
            comList[j] = comList[j].toUpperCase();
            if (input.contains(salList[i])){
                return "P";
            }else if(input.isEmpty()){
            	return "";
            }if (input.contains(comList[j])){
                return "C";
            }
          }
        }
        return "P";
    }

    public static String check_customer_type(String sal, String lGivingSal, String[] salList, String[] typeList, String defaultType){
        if (sal == lGivingSal){
            for (int i = 0; i < salList.length; i++){
                if (sal.contains(salList[i])){
                    return typeList[i];
                }else{
                    continue;
                }
            }
        }
        return defaultType;
    }
}